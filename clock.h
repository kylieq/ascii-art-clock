#ifndef CLOCK_H_
#define CLOCK_H_

typedef struct {
    // TODO: Define your ASCII clock data structure here.
    int *timePtr;
} ClockType;

// Initialize the clock data structure
void initClock(ClockType *clock);

// Print an ASCII clock showing cur_time as the time
void printClock(const time_t cur_time, ClockType *clock);

void print_row_1(ClockType *clock, char x1[4], char x2[4], char x3[4], char x4[4]);
void print_row_2(ClockType *clock, char x1[4], char x2[4], char x3[4], char x4[4]);
void print_row_3(ClockType *clock, char x1[4], char x2[4], char x3[4], char x4[4]);
void print_row_4(ClockType *clock, char x1[4], char x2[4], char x3[4], char x4[4]);
void print_row_5(ClockType *clock, char x1[4], char x2[4], char x3[4], char x4[4]);

// Free up any dynamically allocated memory in the clock
void cleanClock(ClockType *clock);

#endif /* CLOCK_H_ */
