#ifndef TIMER_H_
#define TIMER_H_

typedef struct {
    int *timePtr;
    int minutes;
    int seconds;
} ClockType;

// Initialize the timer with the user-provided input
void initTimer(ClockType *clock, int minutes, int seconds);

// Run the timer -- print out the time each second
void runTimer();

void printClock(ClockType *clock);

void print_row_1(ClockType *clock, char x1[4], char x2[4], char x3[4], char x4[4]);
void print_row_2(ClockType *clock, char x1[4], char x2[4], char x3[4], char x4[4]);
void print_row_3(ClockType *clock, char x1[4], char x2[4], char x3[4], char x4[4]);
void print_row_4(ClockType *clock, char x1[4], char x2[4], char x3[4], char x4[4]);
void print_row_5(ClockType *clock, char x1[4], char x2[4], char x3[4], char x4[4]);

// Clean up memory (as needed)
void cleanTimer(ClockType *clock);

#endif /* TIMER_H_ */
