#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "clock.h"

int main() {
    ClockType *clock;
    const time_t cur_time;
    initClock(clock);
    printClock(cur_time, clock);

    return 0;
}
