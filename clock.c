#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "clock.h"

// Initialize the clock data structure
void initClock(ClockType *clock) {
    clock->timePtr = (int*)malloc(6*sizeof(int));
}


// Print an ASCII clock showing cur_time as the time
void printClock(time_t cur_time, ClockType *clock) {
    // The elements in the following char arrays are what make up the ASCII data clock
    // structure.
    char option_1[4] = {'@', '@', '@', '@'};
    char option_2[4] = {'@', ' ', ' ', '@'};
    char option_3[4] = {' ', ' ', ' ', '@'};
    char option_4[4] = {'@', ' ', ' ', ' '};

    // time() returns the current calendar time measured in seconds.
    time(&cur_time);

    // struct tm *localtime() uses the value pointed by clock to fill a tm structure
    // with the values that represent the corresponding local time.
    struct tm *info;
    info = localtime(&cur_time);

    printf("Current time: %2d:%02d:%02d\n", info->tm_hour, info->tm_min, info->tm_sec);

    // Split the int representing the current hour into its digits.
    int hour = info->tm_hour;
    if (hour<10) {
        clock->timePtr[0] = 0;
        clock->timePtr[1] = hour;
    }
    else {
        int i = 1;
        while (hour>0) {
            int num = hour%10;
            hour/=10;
            clock->timePtr[i] = num;
            i-=1;
        }
    }

    // Split the int representing the current minutes into its digits.
    int minute = info->tm_min;
    if (minute<10) {
        clock->timePtr[2] = 0;
        clock->timePtr[3] = minute;
    }
    else {
        int i = 3;
        while (minute>0) {
            int num = minute%10;
            minute/=10;
            clock->timePtr[i] = num;
            i-=1;
        }
    }

    // Split the int representing the current seconds into its digits.
    int second = info->tm_sec;
    if (second<10) {
        clock->timePtr[4] = 0;
        clock->timePtr[5] = second;
    }
    else {
        int i = 5;
        while (second>0) {
            int num = second%10;
            second/=10;
            clock->timePtr[i] = num;
            i-=1;
        }
    }

    // Call the following print functions to display the ASCII clock data structure.
    print_row_1(clock, option_1, option_2, option_3, option_4);
    printf("\n");
    print_row_2(clock, option_1, option_2, option_3, option_4);
    printf("\n");
    print_row_3(clock, option_1, option_2, option_3, option_4);
    printf("\n");
    print_row_4(clock, option_1, option_2, option_3, option_4);
    printf("\n");
    print_row_5(clock, option_1, option_2, option_3, option_4);
    printf("\n");

    // Free dynamically allocated memory.
    cleanClock(clock);
}


/*   Note on print_row_x functions:
    - I split each number into a 5x4 object. 
*/
void print_row_1(ClockType *clock, char x1[4], char x2[4], char x3[4], char x4[4]) {
    int i = 0;
    int li1[8] = {0,2,3,5,6,7,8,9};

    while (i<6) {
        
        // If the digits representing hour, minute, or second are in the list {0,2,3,5,6,7,8,9},
        // print "@@@@" in the first row.
        for (int j=0; j<8; j++) {
            if (clock->timePtr[i] == li1[j]) {
                for (int k=0; k<4; k++) {
                    printf("%c", x1[k]);
                }
            }
        }

        // If the digits representing hour, minute, or second are equal to 1, print "    @"
        // in the first row.
        if (clock->timePtr[i] == 1) {
            for (int k=0; k<4; k++) {
                printf("%c", x3[k]);
            }
        }

        // If the digits representing hour, minute, or second are equal to 4, print "@  @" in 
        // the first row.
        else if (clock->timePtr[i] == 4) {
            for (int k=0; k<4; k++) {
                printf("%c", x2[k]);
            }
        }

        printf(" ");

        // After every two digits' characters have been printed, print four spaces to distinguish 
        // between hour, minutes, and seconds.
        if ((i%2==1) && (i<5)) printf("   ");

        i += 1;
    }
}


void print_row_2(ClockType *clock, char x1[4], char x2[4], char x3[4], char x4[4]) {
    int i = 0;
    int li1[4] = {0,4,8,9};
    int li2[4] = {1,2,3,7};

    while (i<6) {

        // If the digits representing hour, minute, or second are in {0,4,8,9}, then print
        // "@  @" in the second row.
        for (int j=0; j<4; j++) {
            if (clock->timePtr[i] == li1[j]) {
                for (int k=0; k<4; k++) {
                    printf("%c", x2[k]);
                }
            }
        }

        // If the digits representing hour, minute, or second are in {1,2,3,7}, then print 
        // "   @" in the second row.
        for (int j=0; j<4; j++) {
            if (clock->timePtr[i] == li2[j]) {
                for (int k=0; k<4; k++) {
                    printf("%c", x3[k]);
                }
            }
        }

        // If the digits representing hour, minute, or second are equal to 5 or 6, then print
        // "@   " in the second row.
        if ((clock->timePtr[i] == 5) || (clock->timePtr[i] == 6)) {
            for (int k=0; k<4; k++) {
                printf("%c", x4[k]);
            }
        }

        printf(" ");

        // After every two digits' characters have been printed, print four spaces to distinguish
        // between hour, minutes, and seconds.
        if ((i%2==1) && (i<5)) printf("   ");

        i += 1;
    }
}


void print_row_3(ClockType *clock, char x1[4], char x2[4], char x3[4], char x4[4]) {
    int i = 0;
    int li1[7] = {2,3,4,5,6,8,9};

    while (i<6) {

        // If the digits representing hour, minute, or second are in {2,3,4,5,6,8,9}, then print
        // "@@@@" in the third row.
        for (int j=0; j<7; j++) {
            if (clock->timePtr[i] == li1[j]) {
                for (int k=0; k<4; k++) {
                    printf("%c", x1[k]);
                }
            }
        }

        // If the digits representing hour, minute, or second are equal to 1 or 7, then print 
        // "   @" in the third row.
        if ((clock->timePtr[i] == 1) || (clock->timePtr[i] == 7)) {
            for (int k=0; k<4; k++) {
                printf("%c", x3[k]);
            }
        }

        // If the digits representing hour, minute, or second are equal to 0, then print "@  @"
        // in the third row.
        if (clock->timePtr[i] == 0) {
            for (int k=0; k<4; k++) {
                printf("%c", x2[k]);
            }
        }

        printf(" ");

        // After every two digits' characters have been printed, print " @ " to represent the top of
        // the colon.
        if ((i%2==1) && (i<5)) printf(" @ ");

        i += 1;
    }
}


void print_row_4(ClockType *clock, char x1[4], char x2[4], char x3[4], char x4[4]) {
    int i = 0;
    int li1[6] = {1,3,4,5,7,9};
    int li2[3] = {0,6,8};

    while (i<6) {

        // If the digits representing hour, minute, or second are in {1,3,4,5,7,9}, then print
        // "   @" in the fourth row.
        for (int j=0; j<6; j++) {
            if (clock->timePtr[i] == li1[j]) {
                for (int k=0; k<4; k++) {
                    printf("%c", x3[k]);
                }
            }
        }

        // If the digits representing hour, minute, or second are in {0,6,8}, then print "@  @"
        // in the fourth row.
        for (int j=0; j<3; j++) {
            if (clock->timePtr[i] == li2[j]) {
                for (int k=0; k<4; k++) {
                    printf("%c", x2[k]);
                }
            }
        }

        // If the digits representing hour, minute, or second are equal to 2, then print "@   "
        // in the fourth row.
        if (clock->timePtr[i] == 2) {
            for (int k=0; k<4; k++) {
                printf("%c", x4[k]);
            }
        }

        printf(" ");

        // After every two digits' characters have been printed, print " @ " to represent the bottom
        // of the colon.
        if ((i%2==1) && (i<5)) printf(" @ ");

        i += 1;
    }
}


void print_row_5(ClockType *clock, char x1[4], char x2[4], char x3[4], char x4[4]) {
    int i = 0;
    int li1[6] = {0,2,3,5,6,8};
    int li2[4] = {1,4,7,9};

    while (i<6) {

        // If the digits representing hour, minute, or second are in {0,2,3,5,6,8}, then print
        // "@@@@" in the fifth row.
        for (int j=0; j<6; j++) {
            if (clock->timePtr[i] == li1[j]) {
                for (int k=0; k<4; k++) {
                    printf("%c", x1[k]);
                }
            }
        }

        // If the digits representing hour, minute, or second are in {1,4,7,9}, then print "   @"
        // in the fifth row.
        for (int j=0; j<4; j++) {
            if (clock->timePtr[i] == li2[j]) {
                for (int k=0; k<4; k++) {
                    printf("%c", x3[k]);
                }
            }
        }

        printf(" ");

        // After every two digits' characters have been printed, print four spaces to distinguish
        // between hour, minutes, and seconds.
        if ((i%2==1) && (i<5)) printf("   ");

        i += 1;
    }
}


// Free up any dynamically allocated memory in the clock
void cleanClock(ClockType *clock) {
    free(clock->timePtr);
}
